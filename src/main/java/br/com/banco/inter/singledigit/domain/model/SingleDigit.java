package br.com.banco.inter.singledigit.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class SingleDigit implements AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @Lob
    @NotNull
    @Column
    private String digit;

    @NotNull
    @Column
    private Integer concatenation;

    @NotNull
    @Column(nullable = false)
    private Integer digitCalculated;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonIgnore
    private User user;

    public boolean isEquals(SingleDigit singleDigit) {
        return singleDigit.getDigit().equals(this.digit) && singleDigit.getConcatenation().equals(this.concatenation);
    }

}
