package br.com.banco.inter.singledigit.util;

import br.com.banco.inter.singledigit.domain.model.SingleDigit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SingleDigitCacheUtil {

    private static final List<SingleDigit> singleDigitList = new ArrayList<>(10);

    public void addItem(SingleDigit singleDigit) {
        if (singleDigitList.size() == 10) {
            singleDigitList.remove(0);
        }

        singleDigitList.add(singleDigit);
    }

    public SingleDigit searchItem(SingleDigit singleDigit) {
        return singleDigitList.stream().filter(e -> e.isEquals(singleDigit)).findFirst().orElse(null);
    }
}
