package br.com.banco.inter.singledigit.service;

import br.com.banco.inter.singledigit.domain.model.User;
import br.com.banco.inter.singledigit.domain.repository.SingleDigitRepository;
import br.com.banco.inter.singledigit.domain.repository.UserRepository;
import br.com.banco.inter.singledigit.error.exceptions.ResourceNotFoundException;
import br.com.banco.inter.singledigit.valitation.UserValidation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Transactional
public class UserService {

    private final UserRepository userRepository;
    private final UserValidation userValidation;
    private final SingleDigitRepository singleDigitRepository;

    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    public Iterable<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    public User save(User user) {
        userValidation.validate(user);
        return userRepository.save(user);
    }

    public User saveCryptography(User user) {
        return userRepository.save(user);
    }

    public void delete(Long id) {
        singleDigitRepository.updateUserIdByUserId(id);
        userRepository.deleteById(id);
    }

    public User validUser(Long id) {
        Optional<User> user = userRepository.findById(id);

        if (user.isPresent()) {
            return user.get();
        } else {
            throw new ResourceNotFoundException("user;O usuário informado é inexistente");
        }
    }

}
