package br.com.banco.inter.singledigit.valitation;

import br.com.banco.inter.singledigit.domain.model.SingleDigit;
import br.com.banco.inter.singledigit.error.exceptions.InvalidArgumentException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
@Slf4j
public class SingleDigitValidation {

    public void validate(SingleDigit singleDigit) {
        validDigitType(singleDigit.getDigit());
        validDigitSize(singleDigit.getDigit());
    }

    private void validDigitType(String digit) {
        if (! StringUtils.isNumeric(digit)) {
            throw new InvalidArgumentException("digit;O digito informado possui caracteres não numéricos");
        }
    }

    private void validDigitSize(String digit) {
        BigInteger maxValue = BigInteger.valueOf(10).pow(1000000);
        BigInteger digitParam = new BigInteger(digit);
        int compare = maxValue.compareTo(digitParam);

        if (compare < 0) {
            throw new InvalidArgumentException("digit;O tamanho do dígito excede o valor permitido de 10^1000000");
        }

        if (digitParam.compareTo(BigInteger.valueOf(1)) < 0) {
            throw new InvalidArgumentException("digit;O valor do campo 'dígito' não pode ser menor que 1");
        }
    }

}
