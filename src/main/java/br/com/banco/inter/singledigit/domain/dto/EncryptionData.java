package br.com.banco.inter.singledigit.domain.dto;

import lombok.*;

import java.security.KeyPair;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EncryptionData {

    private KeyPair keyPair;

    private Boolean encrypted;

}
