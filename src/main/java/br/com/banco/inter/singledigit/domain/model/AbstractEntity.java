package br.com.banco.inter.singledigit.domain.model;

import java.io.Serializable;

public interface AbstractEntity extends Serializable {

    Long getId();

}
