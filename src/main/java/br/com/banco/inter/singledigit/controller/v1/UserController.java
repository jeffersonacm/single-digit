package br.com.banco.inter.singledigit.controller.v1;

import br.com.banco.inter.singledigit.domain.dto.UserDto;
import br.com.banco.inter.singledigit.domain.model.User;
import br.com.banco.inter.singledigit.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping(value = "/v1/user")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    private final UserService userService;

    @GetMapping(value = "{userId}")
    @ApiOperation(value = "Busca um usuário específico pelo seu ID")
    public ResponseEntity<User> find(@PathVariable Long userId) {
        Optional<User> user = userService.findById(userId);

        if (user.isPresent()) {
            return ResponseEntity.ok(user.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping()
    @ApiOperation(value = "Busca a lista de usuários")
    public ResponseEntity<Iterable<User>> findAll(Pageable pageable) {
        return ResponseEntity.ok(userService.findAll(pageable));
    }

    @PostMapping
    @ApiOperation(value = "Salva um usuário")
    public ResponseEntity save(@Valid @RequestBody UserDto userDto) {
        User userSave = userService.save(userDto.toUser());

        URI location = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(userSave.getId())
            .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(value = "{userId}")
    @ApiOperation(value = "Atualiza os dados do usuário")
    public ResponseEntity update(@Valid @RequestBody UserDto userDto, @PathVariable Long userId) {
        Optional<User> userSaved = userService.findById(userId);

        if (userSaved.isPresent()) {
            User user = userDto.toUser();
            user.setId(userId);
            userService.save(user);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "{userId}")
    @ApiOperation(value = "Remove um usuário")
    public ResponseEntity delete(@PathVariable Long userId) {
        Optional<User> user = userService.findById(userId);

        if (user.isPresent()) {
            userService.delete(userId);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
