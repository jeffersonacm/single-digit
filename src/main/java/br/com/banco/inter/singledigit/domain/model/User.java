package br.com.banco.inter.singledigit.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User implements AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    @Column(length = 512)
    private String name;

    @NotNull
    @Column(length = 512, unique = true)
    private String email;

    @OneToMany(targetEntity = SingleDigit.class, fetch = FetchType.EAGER)
    @JsonIgnore
    private List<SingleDigit> singleDigit;

}
