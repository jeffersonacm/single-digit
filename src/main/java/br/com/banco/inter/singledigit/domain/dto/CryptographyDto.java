package br.com.banco.inter.singledigit.domain.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CryptographyDto {

    @NotEmpty(message = "O valor do campo 'chave publica' é obrigatório")
    private String publicKey;

}
