package br.com.banco.inter.singledigit.error.handler;

import br.com.banco.inter.singledigit.error.exceptions.ErrorDetails;
import br.com.banco.inter.singledigit.error.exceptions.InvalidArgumentException;
import br.com.banco.inter.singledigit.error.exceptions.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity handleResourceNotFoundException(ResourceNotFoundException rnfException) {
        ErrorDetails errorDetails = ErrorDetails.ErrorDetailsBuilder
                .newBuilder()
                .timestamp(new Date().getTime())
                .status(HttpStatus.NOT_FOUND.value())
                .title("Recurso não encontrado")
                .message(rnfException.getMessage())
                .build();
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidArgumentException.class)
        public ResponseEntity handleInvalidArgumentException(InvalidArgumentException rnfException) {
            String msg = rnfException.getMessage();
            String [] msgSplit = msg.split(";");
            String field = "";

            if (msgSplit.length == 2) {
                field = msgSplit[0];
                msg = msgSplit[1];
            }

            ErrorDetails errorDetails = ErrorDetails.ErrorDetailsBuilder
                    .newBuilder()
                    .timestamp(new Date().getTime())
                    .status(HttpStatus.BAD_REQUEST.value())
                    .title("Erro na validação dos campos")
                    .field(field)
                    .message(msg)
                    .build();
            return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity handleMethodArgumentNotValidException(MethodArgumentNotValidException manvException) {
        List<FieldError> fieldErrors = manvException.getBindingResult().getFieldErrors();
        String fields = fieldErrors.stream().map(FieldError::getField).collect(Collectors.joining(","));
        String messages = fieldErrors.stream().map(FieldError::getDefaultMessage).collect(Collectors.joining(","));

        ErrorDetails errorDetails = ErrorDetails.ErrorDetailsBuilder
                .newBuilder()
                .timestamp(new Date().getTime())
                .status(HttpStatus.BAD_REQUEST.value())
                .title("Erro na validação dos campos")
                .field(fields)
                .message(messages)
                .build();

        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

}
