package br.com.banco.inter.singledigit.controller.v1;

import br.com.banco.inter.singledigit.domain.dto.SingleDigitDto;
import br.com.banco.inter.singledigit.domain.model.SingleDigit;
import br.com.banco.inter.singledigit.service.SingleDigitService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/v1/singledigit")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SingleDigitController {

    private final SingleDigitService singleDigitService;

    @PostMapping
    @ApiOperation(value = "Calcula o dígito único")
    public ResponseEntity<SingleDigit> calculateDigit(@Valid @RequestBody SingleDigitDto singleDigitDto) {
        return ResponseEntity.created(null).body(singleDigitService.save(singleDigitDto.toSingleDigit()));
    }

    @PostMapping(value = "/user/{userId}")
    @ApiOperation(value = "Calcula o dígito único vinculado à um usuário")
    public ResponseEntity<SingleDigit> calculateDigitUser(@Valid @RequestBody SingleDigitDto singleDigitDto,
                                             @PathVariable Long userId) {
        return ResponseEntity.created(null)
                .body(singleDigitService.saveWithUser(singleDigitDto.toSingleDigit(), userId));
    }

    @GetMapping(value = "/user/{userId}")
    @ApiOperation(value = "Busca uma lista de dígitos por usuário")
    public ResponseEntity<Iterable<SingleDigit>> findDigitByUser(@PathVariable Long userId) {
        return ResponseEntity.ok(singleDigitService.findAllByUser(userId));
    }

}
