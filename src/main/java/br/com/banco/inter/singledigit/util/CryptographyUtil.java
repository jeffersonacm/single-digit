package br.com.banco.inter.singledigit.util;

import br.com.banco.inter.singledigit.domain.dto.CryptographyDto;
import br.com.banco.inter.singledigit.domain.dto.EncryptionData;
import br.com.banco.inter.singledigit.domain.model.User;
import br.com.banco.inter.singledigit.error.exceptions.InvalidArgumentException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.*;
import java.util.Base64;
import java.util.HashMap;

@Service
@Slf4j
public class CryptographyUtil {

    private static final int KEY_SIZE = 2048;
    private static final HashMap<Long, EncryptionData> longKeyPairHashMap = new HashMap<>();
    private static SecretKey aesKey;
    private static final String MSG_PUBLIC_KEY = "publicKey;Não foi possível encryptar os dados, verifique a chave pública";
    private static final String MSG_SIZE_DATA = "publicKey;Não foi possível encryptar os dados, tamanho dos mesmos ultrapassam os limites da criptografia";
    private static final String MSG_INVALID_PUBLIC_KEY = "publicKey;A chave pública informada não é válida";
    private static final String AES = "AES";
    private static final String RSA = "RSA";

    private KeyPair generateKeyPair(Long userId) throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance(RSA);
        keyGen.initialize(KEY_SIZE);
        KeyPair pair = keyGen.generateKeyPair();

        EncryptionData encryptionData = EncryptionData.builder().keyPair(pair).encrypted(false).build();
        longKeyPairHashMap.put(userId, encryptionData);

        return pair;
    }

    @SneakyThrows
    public CryptographyDto getUserPublicKey(Long userId) {
        KeyPair keyPair = null;
        EncryptionData encryptionData = longKeyPairHashMap.get(userId);

        if (encryptionData == null) {
            keyPair = generateKeyPair(userId);
        } else {
            keyPair = encryptionData.getKeyPair();
        }

        return CryptographyDto.builder()
                .publicKey(Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()))
                .build();
    }

    public User encryptUser(User user, CryptographyDto cryptographyDto) {
        EncryptionData encryptionData = longKeyPairHashMap.get(user.getId());

        if (encryptionData != null && Boolean.TRUE.equals(encryptionData.getEncrypted())) {
            throw new InvalidArgumentException("Os dados do usuário já estão encryptados");
        }

        PublicKey publicKeyStorage = validPublicKey(user, cryptographyDto.getPublicKey());

        user.setName(doEncryptData(user.getName(), publicKeyStorage));
        user.setEmail(doEncryptData(user.getEmail(), publicKeyStorage));

        encryptionData.setEncrypted(true);

        longKeyPairHashMap.put(user.getId(), encryptionData);

        return user;
    }

    private String doEncryptData(String data, PublicKey publicKeyStorage) {
        try {
            Cipher cipher = Cipher.getInstance(RSA);
            cipher.init(Cipher.ENCRYPT_MODE, publicKeyStorage);


            KeyGenerator generator = KeyGenerator.getInstance(AES);
            generator.init(128);

            if (aesKey == null) {
                aesKey = generator.generateKey();
            }

            Cipher aesCipher = Cipher.getInstance(AES);
            aesCipher.init(Cipher.ENCRYPT_MODE, aesKey);

            byte[] aesData = aesCipher.doFinal(data.getBytes());
            byte[] encrypt = cipher.doFinal(aesData);

            return Base64.getEncoder().encodeToString(encrypt);
        } catch (IllegalBlockSizeException ex) {
            throw new InvalidArgumentException(MSG_SIZE_DATA);
        } catch (Exception e) {
            throw new InvalidArgumentException(MSG_PUBLIC_KEY);
        }
    }

    public User decrypt(User user, CryptographyDto cryptographyDto) {
        validPublicKey(user, cryptographyDto.getPublicKey());

        EncryptionData encryptionData = longKeyPairHashMap.get(user.getId());

        if (Boolean.TRUE.equals(encryptionData.getEncrypted())) {
            PrivateKey privateKey = encryptionData.getKeyPair().getPrivate();

            user.setEmail(doDecrypt(user.getEmail(), privateKey));
            user.setName(doDecrypt(user.getName(), privateKey));

            encryptionData.setEncrypted(false);
            longKeyPairHashMap.put(user.getId(), encryptionData);

            return user;
        } else {
            throw new InvalidArgumentException("Os dados do usuário já estão decryptados");
        }
    }

    private String doDecrypt(String data, PrivateKey privateKey) {
        try {
            Cipher cipher = Cipher.getInstance(RSA);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            
            byte[] bytes = cipher.doFinal(Base64.getDecoder().decode(data));

            Cipher aesCipher = Cipher.getInstance(AES);
            aesCipher.init(Cipher.DECRYPT_MODE, aesKey);
            byte[] bytePlainText = aesCipher.doFinal(bytes);

            return new String(bytePlainText, "UTF8");

        } catch (IllegalBlockSizeException ex) {
            throw new InvalidArgumentException(MSG_SIZE_DATA);
        } catch (Exception e) {
            throw new InvalidArgumentException(MSG_PUBLIC_KEY);
        }
    }

    private PublicKey validPublicKey(User user, String publicKey) {
        if (longKeyPairHashMap.get(user.getId()) == null) {
            throw new InvalidArgumentException("publicKey;Não foi possível encontar chave pública para o usuário");
        }

        PublicKey publicKeyStorage = longKeyPairHashMap.get(user.getId()).getKeyPair().getPublic();

        if (! Base64.getEncoder().encodeToString(publicKeyStorage.getEncoded()).equals(publicKey)) {
            throw new InvalidArgumentException(MSG_INVALID_PUBLIC_KEY);
        }

        return publicKeyStorage;
    }

    public EncryptionData getUserEncryptionData(Long userId) {
        return longKeyPairHashMap.get(userId);
    }

}
