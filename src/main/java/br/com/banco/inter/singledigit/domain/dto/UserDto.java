package br.com.banco.inter.singledigit.domain.dto;

import br.com.banco.inter.singledigit.domain.model.User;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserDto {

    @NotEmpty(message = "O valor do campo 'name' é obrigatório")
    @Size(max = 128, message = "O valor máximo para o nome é de 128 caracteres")
    private String name;

    @Email(message = "O email informado não é válido")
    @NotEmpty(message = "O valor do campo 'email' é obrigatório")
    @Size(max = 254, message = "O valor máximo para o email é de 254 caracteres")
    private String email;

    public User toUser() {
        return User.builder().name(this.name).email(this.email).build();
    }

}
