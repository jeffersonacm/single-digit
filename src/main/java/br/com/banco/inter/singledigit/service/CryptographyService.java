package br.com.banco.inter.singledigit.service;

import br.com.banco.inter.singledigit.domain.dto.CryptographyDto;
import br.com.banco.inter.singledigit.domain.model.User;
import br.com.banco.inter.singledigit.util.CryptographyUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CryptographyService {

    private final UserService userService;

    private final CryptographyUtil cryptographyUtil;

    public CryptographyDto getUserPublicKey(Long userId) {
        userService.validUser(userId);
        return cryptographyUtil.getUserPublicKey(userId);
    }

    public void encryptUser(Long userId, CryptographyDto cryptographyDto) {
        User user = userService.validUser(userId);

        User userCrypt = cryptographyUtil.encryptUser(user, cryptographyDto);
        userService.saveCryptography(userCrypt);
    }

    public void decryptUser(Long userId, CryptographyDto cryptographyDto) {
        User user = userService.validUser(userId);

        User userDecrypted = cryptographyUtil.decrypt(user, cryptographyDto);
        userService.saveCryptography(userDecrypted);
    }
}
