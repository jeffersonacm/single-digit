package br.com.banco.inter.singledigit.valitation;

import br.com.banco.inter.singledigit.domain.dto.EncryptionData;
import br.com.banco.inter.singledigit.domain.model.User;
import br.com.banco.inter.singledigit.domain.repository.UserRepository;
import br.com.banco.inter.singledigit.error.exceptions.InvalidArgumentException;
import br.com.banco.inter.singledigit.util.CryptographyUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserValidation {

    private final CryptographyUtil cryptographyUtil;

    private final UserRepository userRepository;

    public void validate(User user) {
        validateEmail(user);
        validateCryptography(user);
    }

    private void validateEmail(User user) {
        if (! EmailValidator.getInstance().isValid(user.getEmail())) {
            throw new InvalidArgumentException("email;O email informado não é válido");
        }

        Optional<User> userByEmail = userRepository.findUserByEmail(user.getEmail());

        if (userByEmail.isPresent() && ! userByEmail.get().getId().equals(user.getId())) {
            throw new InvalidArgumentException("email;O email informado não está disponível");
        }
    }

    private void validateCryptography(User user) {
        if (user.getId() != null) {
            EncryptionData userEncryptionData = cryptographyUtil.getUserEncryptionData(user.getId());

            if (userEncryptionData != null && userEncryptionData.getEncrypted()) {
                throw new InvalidArgumentException("Não foi possível atualizar os dados do usuário, pois os dados estão criptografados");
            }
        }
    }

}
