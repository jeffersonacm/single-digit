package br.com.banco.inter.singledigit.domain.dto;

import br.com.banco.inter.singledigit.domain.model.SingleDigit;
import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SingleDigitDto {

    @NotEmpty(message = "O valor do campo 'dígito' é obrigatório")
    private String digit;

    @NotNull(message = "O valor do campo 'concatenação' é obrigatório")
    @Min(value = 1, message = "O valor do campo 'concatenação' não pode ser menor que 1")
    @Max(value = 100000, message = "O valor do campo 'concatenação' não pode ser maior que 100000")
    private Integer concatenation;

    public SingleDigit toSingleDigit() {
        return SingleDigit.builder().digit(this.digit).concatenation(this.concatenation).build();
    }

}
