package br.com.banco.inter.singledigit.domain.repository;

import br.com.banco.inter.singledigit.domain.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    Optional<User> findUserByEmail(String email);

}
