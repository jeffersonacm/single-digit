package br.com.banco.inter.singledigit.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CalculateDigitUtil {

    public Integer calculate(String digit, int concatenation) {
        int sum = 0;
        for(int i = 0; i < concatenation; i++) {
            sum += sumDigits(digit);
        }

        if (sum > 9) {
            sum = sumDigits(String.valueOf(sum));
        }

        return sum;
    }

    public Integer sumDigits(String digit) {
        Integer sumDigits = 0;
        char[] chars = digit.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            sumDigits += Integer.parseInt(String.valueOf(chars[i]));
        }

        if (sumDigits > 9) {
            return sumDigits(sumDigits.toString());
        }

        return sumDigits;
    }

}
