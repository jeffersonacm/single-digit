package br.com.banco.inter.singledigit.controller.v1;

import br.com.banco.inter.singledigit.domain.dto.CryptographyDto;
import br.com.banco.inter.singledigit.service.CryptographyService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(value = "/v1/cryptography")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CryptographyController {

    private final CryptographyService cryptographyService;

    @GetMapping(value = "{userId}")
    @ApiOperation(value = "Retorna a chave publica do usuário")
    public ResponseEntity<CryptographyDto> getKey(@PathVariable Long userId) {
        return ResponseEntity.ok().body(cryptographyService.getUserPublicKey(userId));
    }

    @PutMapping(value = "/encrypt/{userId}")
    @ApiOperation(value = "Criptografa os dados do usuário usando a chave publica")
    public ResponseEntity encrypt(@Valid @PathVariable Long userId, @RequestBody CryptographyDto cryptographyDto) {
        cryptographyService.encryptUser(userId, cryptographyDto);
        return ResponseEntity.noContent().build();
    }

    @PutMapping(value = "/decrypt/{userId}")
    @ApiOperation(value = "Descriptografa os dados do usuário")
    public ResponseEntity decrypt(@PathVariable Long userId, @RequestBody CryptographyDto cryptographyDto) {
        cryptographyService.decryptUser(userId, cryptographyDto);
        return ResponseEntity.noContent().build();
    }
}
