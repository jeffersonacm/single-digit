package br.com.banco.inter.singledigit.domain.repository;

import br.com.banco.inter.singledigit.domain.model.SingleDigit;
import br.com.banco.inter.singledigit.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface SingleDigitRepository extends JpaRepository<SingleDigit, Long> {

    Iterable<SingleDigit> findAllByUser(User user);

    @Modifying
    @Query(value = "update single_digit set user_id = null where user_id = :userId", nativeQuery = true)
    void updateUserIdByUserId(@Param("userId") Long userId);

}
