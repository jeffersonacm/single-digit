package br.com.banco.inter.singledigit.service;

import br.com.banco.inter.singledigit.domain.model.SingleDigit;
import br.com.banco.inter.singledigit.domain.model.User;
import br.com.banco.inter.singledigit.domain.repository.SingleDigitRepository;
import br.com.banco.inter.singledigit.util.CalculateDigitUtil;
import br.com.banco.inter.singledigit.util.SingleDigitCacheUtil;
import br.com.banco.inter.singledigit.valitation.SingleDigitValidation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SingleDigitService {

    private final SingleDigitRepository singleDigitRepository;

    private final CalculateDigitUtil calculateDigitUtil;

    private final SingleDigitCacheUtil singleDigitCacheUtil;

    private final SingleDigitValidation singleDigitValidation;

    private final UserService userService;

    public SingleDigit save(SingleDigit singleDigit) {
        singleDigitValidation.validate(singleDigit);

        SingleDigit singleDigitFind = singleDigitCacheUtil.searchItem(singleDigit);

        Integer digitCalculated = null;

        if (singleDigitFind != null) {
            digitCalculated = singleDigitFind.getDigitCalculated();
            log.info("Digit find in cache: " + digitCalculated);
        } else {
            digitCalculated = calculateDigitUtil.calculate(singleDigit.getDigit(), singleDigit.getConcatenation());
            singleDigitCacheUtil.addItem(singleDigit);
        }

        singleDigit.setDigitCalculated(digitCalculated);
        return singleDigitRepository.save(singleDigit);
    }

    public SingleDigit saveWithUser(SingleDigit singleDigit, Long id) {
        User user = userService.validUser(id);
        singleDigit.setUser(user);

        return this.save(singleDigit);
    }

    public Iterable<SingleDigit> findAllByUser(Long id) {
        User user = userService.validUser(id);

        return singleDigitRepository.findAllByUser(user);
    }

}
