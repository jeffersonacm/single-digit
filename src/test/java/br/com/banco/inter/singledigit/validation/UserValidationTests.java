package br.com.banco.inter.singledigit.validation;

import br.com.banco.inter.singledigit.domain.dto.CryptographyDto;
import br.com.banco.inter.singledigit.domain.model.User;
import br.com.banco.inter.singledigit.domain.repository.UserRepository;
import br.com.banco.inter.singledigit.error.exceptions.InvalidArgumentException;
import br.com.banco.inter.singledigit.util.CryptographyUtil;
import br.com.banco.inter.singledigit.valitation.UserValidation;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserValidationTests {

    private final UserValidation userValidation;

    private final CryptographyUtil cryptographyUtil;

    private final UserRepository userRepository;

    @Test
    public void validateUserValidParams() {
        User user = User.builder().email("person@gmail.com").name("person").build();
        userValidation.validate(user);
    }

    @Test
    public void validateUserInvalidEmailFormatExpectThrowInvalidArgumentException() {
        User user = User.builder().email("persongmail.com").name("person").build();

        Assertions.assertThrows(InvalidArgumentException.class, () -> userValidation.validate(user));
    }

    @Test
    public void validateUserInvalidEmailNotUniqueExpectThrowInvalidArgumentException() {
        User user = User.builder().email("persongmail.com").name("person").build();

        userRepository.save(user);

        Assertions.assertThrows(InvalidArgumentException.class, () -> userValidation.validate(user));
    }

    @Test
    public void validateUserUpdateCryptedDataExpectInvalidArgumentException() {
        User user = User.builder().id(1L).email("person@gmail.com").name("person").build();

        CryptographyDto userPublicKey = cryptographyUtil.getUserPublicKey(1L);

        cryptographyUtil.encryptUser(user, userPublicKey);

        Assertions.assertThrows(InvalidArgumentException.class, () -> userValidation.validate(user));
    }
}
