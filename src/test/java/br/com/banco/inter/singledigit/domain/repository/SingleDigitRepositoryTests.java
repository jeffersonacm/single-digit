package br.com.banco.inter.singledigit.domain.repository;

import br.com.banco.inter.singledigit.domain.model.SingleDigit;
import br.com.banco.inter.singledigit.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;

import javax.validation.ConstraintViolationException;
import java.util.Collection;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SingleDigitRepositoryTests {

    private final SingleDigitRepository singleDigitRepository;
    private final UserRepository userRepository;

    @Test
    @Order(1)
    public void findAllDigitsShouldReturnTwoObjects() {
        SingleDigit singleDigit = SingleDigit.builder().digit("1").digitCalculated(1).concatenation(1).build();
        singleDigitRepository.save(singleDigit);

        SingleDigit singleDigit2 = SingleDigit.builder().digit("1").digitCalculated(1).concatenation(1).build();
        singleDigitRepository.save(singleDigit2);

        Iterable<SingleDigit> all = singleDigitRepository.findAll();
        Assertions.assertEquals(2, ((Collection<?>) all).size());
    }

    @Test
    @Order(2)
    public void saveDigitShouldPersist() {
        SingleDigit singleDigit = SingleDigit.builder().digit("1").digitCalculated(1).concatenation(1).build();
        singleDigitRepository.save(singleDigit);

        Assertions.assertNotNull(singleDigit.getId());
        Assertions.assertEquals("1", singleDigit.getDigit());
        Assertions.assertEquals(1, singleDigit.getConcatenation());
        Assertions.assertEquals(1, singleDigit.getDigitCalculated());
    }

    @Test
    @Order(3)
    public void saveDigitWithInvalidUserExpectErrorInvalidThrowConstraintViolationException() {
        User user = User.builder().id(1L).email("person@prso").name("name").build();
        SingleDigit singleDigit = SingleDigit.builder().digit("1").digitCalculated(1).concatenation(1).user(user).build();

        Assertions.assertThrows(DataIntegrityViolationException.class, () -> singleDigitRepository.save(singleDigit));
    }

    @Test
    @Order(4)
    public void saveDigitExpectErrorNullDigitThrowConstraintViolationException() {
        SingleDigit singleDigit = SingleDigit.builder().digit(null).digitCalculated(1).concatenation(1).build();

        Assertions.assertThrows(ConstraintViolationException.class, () -> singleDigitRepository.save(singleDigit));
    }

    @Test
    @Order(5)
    public void saveDigitExpectErrorNullConcatenationThrowConstraintViolationException() {
        SingleDigit singleDigit = SingleDigit.builder().digit("1").digitCalculated(1).concatenation(null).build();

        Assertions.assertThrows(ConstraintViolationException.class, () -> singleDigitRepository.save(singleDigit));
    }

    @Test
    @Order(6)
    public void saveDigitExpectErrorNullDigitGeneratedThrowConstraintViolationException() {
        SingleDigit singleDigit = SingleDigit.builder().digit("1").digitCalculated(null).concatenation(1).build();

        Assertions.assertThrows(ConstraintViolationException.class, () -> singleDigitRepository.save(singleDigit));
    }

    @Test
    @Order(7)
    public void findAllDigitsByUserShouldReturnTwoObjects() {
        User user = User.builder().name("name").email("person@hotmail.com").build();
        User user2 = User.builder().name("name").email("person@gmail.com").build();

        userRepository.save(user);
        userRepository.save(user2);

        SingleDigit singleDigit = SingleDigit.builder().digit("1").digitCalculated(1).concatenation(1).user(user).build();
        singleDigitRepository.save(singleDigit);

        SingleDigit singleDigit2 = SingleDigit.builder().digit("1").digitCalculated(1).concatenation(1).user(user).build();
        singleDigitRepository.save(singleDigit2);

        SingleDigit singleDigit3 = SingleDigit.builder().digit("1").digitCalculated(1).concatenation(1).user(user2).build();
        singleDigitRepository.save(singleDigit3);

        Iterable<SingleDigit> all = singleDigitRepository.findAllByUser(user);
        Assertions.assertEquals(2, ((Collection<?>) all).size());
    }

}
