package br.com.banco.inter.singledigit.util;

import br.com.banco.inter.singledigit.domain.dto.CryptographyDto;
import br.com.banco.inter.singledigit.domain.model.User;
import br.com.banco.inter.singledigit.error.exceptions.InvalidArgumentException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.transaction.annotation.Transactional;

@ExtendWith(CryptographyUtilTests.FooParameterResolver.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CryptographyUtilTests {

    @Test
    @Order(1)
    public void getPublicKey(CryptographyUtil cryptographyUtil) {
        CryptographyDto userPublicKey = cryptographyUtil.getUserPublicKey(120L);
        Assertions.assertNotNull(userPublicKey.getPublicKey());
    }

    @Test
    @Order(2)
    @Transactional
    public void encryptUserData(CryptographyUtil cryptographyUtil) {
        User user = User.builder().id(121L).name("jefferson").email("jefferson@jefferson").build();

        CryptographyDto userPublicKey = cryptographyUtil.getUserPublicKey(121L);
        User userEncrypt = cryptographyUtil.encryptUser(user, userPublicKey);

        Assertions.assertNotEquals("jefferson", userEncrypt.getEmail());
        Assertions.assertNotEquals("jefferson@jefferson", userEncrypt.getName());
    }

    @Test
    @Order(3)
    @Transactional
    public void encryptUserDataUserDidNotGenerateKey(CryptographyUtil cryptographyUtil) {
        User user = User.builder().id(122L).name("jefferson").email("jefferson@jefferson").build();

        CryptographyDto userPublicKey = cryptographyUtil.getUserPublicKey(1111L);

        Assertions.assertThrows(InvalidArgumentException.class, () -> cryptographyUtil.encryptUser(user, userPublicKey));
    }

    @Test
    @Order(4)
    public void decryptUserData(CryptographyUtil cryptographyUtil) {
        User user = User.builder().id(123L).name("jefferson").email("jefferson@jefferson").build();

        CryptographyDto userPublicKey = cryptographyUtil.getUserPublicKey(123L);
        User userEncrypt = cryptographyUtil.encryptUser(user, userPublicKey);

        User userDecrypt = cryptographyUtil.decrypt(userEncrypt, userPublicKey);

        Assertions.assertEquals("jefferson@jefferson", userDecrypt.getEmail());
        Assertions.assertEquals("jefferson", userDecrypt.getName());
    }

    @Test
    @Order(5)
    public void decryptUserDataUserDidNotGenerateKeyExpectErrorInvalidArgumentException(CryptographyUtil cryptographyUtil) {
        User user = User.builder().id(124L).name("jefferson").email("jefferson@jefferson").build();

        CryptographyDto userPublicKey = cryptographyUtil.getUserPublicKey(1232L);

        Assertions.assertThrows(InvalidArgumentException.class, () -> cryptographyUtil.decrypt(user, userPublicKey));
    }

    @Test
    @Order(6)
    public void encryptUserDataUserInvalidKeyExpectErrorInvalidArgumentException(CryptographyUtil cryptographyUtil) {
        User user = User.builder().id(125L).name("jefferson").email("jefferson@jefferson").build();

        CryptographyDto userPublicKey = cryptographyUtil.getUserPublicKey(125L);
        userPublicKey.setPublicKey("invalid");
        Assertions.assertThrows(InvalidArgumentException.class, () -> cryptographyUtil.encryptUser(user, userPublicKey));
    }

    @Test
    @Order(7)
    public void decryptUserDataUserInvalidkeyExpectErrorInvalidArgumentException(CryptographyUtil cryptographyUtil) {
        User user = User.builder().id(126L).name("jefferson").email("jefferson@jefferson").build();

        CryptographyDto userPublicKey = cryptographyUtil.getUserPublicKey(126L);
        cryptographyUtil.encryptUser(user, userPublicKey);

        userPublicKey.setPublicKey("invalid");
        Assertions.assertThrows(InvalidArgumentException.class, () -> cryptographyUtil.decrypt(user, userPublicKey));
    }

    @Test
    @Order(8)
    @Transactional
    public void encryptUserDataUserAlreadyEncryptedExpectErrorInvalidArgumentException(CryptographyUtil cryptographyUtil) {
        User user = User.builder().id(127L).name("jefferson").email("jefferson@jefferson").build();

        CryptographyDto userPublicKey = cryptographyUtil.getUserPublicKey(127L);
        cryptographyUtil.encryptUser(user, userPublicKey);

        Assertions.assertThrows(InvalidArgumentException.class, () -> cryptographyUtil.encryptUser(user, userPublicKey));
    }

    @Test
    @Order(9)
    public void decryptUserDataUserAlreadyDecryptedExpectErrorInvalidArgumentException(CryptographyUtil cryptographyUtil) {
        User user = User.builder().id(128L).name("jefferson").email("jefferson@jefferson").build();

        CryptographyDto userPublicKey = cryptographyUtil.getUserPublicKey(128L);
        cryptographyUtil.encryptUser(user, userPublicKey);
        cryptographyUtil.decrypt(user, userPublicKey);

        Assertions.assertThrows(InvalidArgumentException.class, () -> cryptographyUtil.decrypt(user, userPublicKey));
    }

    @Test
    @Order(10)
    public void encryptUserDataUserMaxLengthSupportFiedlExpectErrorInvalidArgumentException(CryptographyUtil cryptographyUtil) {
        String randomName = RandomStringUtils.random(128);
        String randomEmail = RandomStringUtils.random(254);

        User user = User
                .builder()
                .id(130L)
                .name(randomName)
                .email(randomEmail).build();

        CryptographyDto userPublicKey = cryptographyUtil.getUserPublicKey(130L);

        Assertions.assertThrows(InvalidArgumentException.class, () -> cryptographyUtil.encryptUser(user, userPublicKey));
    }

    static class FooParameterResolver implements ParameterResolver {
        @Override
        public boolean supportsParameter(ParameterContext parameterContext,
                                         ExtensionContext extensionContext) throws ParameterResolutionException {
            return parameterContext.getParameter().getType() == CryptographyUtil.class;
        }

        @Override
        public Object resolveParameter(ParameterContext parameterContext,
                                       ExtensionContext extensionContext) throws ParameterResolutionException {
            return new CryptographyUtil();
        }
    }
}
