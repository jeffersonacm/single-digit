package br.com.banco.inter.singledigit.validation;

import br.com.banco.inter.singledigit.domain.model.SingleDigit;
import br.com.banco.inter.singledigit.error.exceptions.InvalidArgumentException;
import br.com.banco.inter.singledigit.valitation.SingleDigitValidation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.*;

import java.math.BigInteger;

@ExtendWith(SingleDigitValidationTests.FooParameterResolver.class)
public class SingleDigitValidationTests {

    @Test
    public void validateDigitType(SingleDigitValidation singleDigitValidation) {
        SingleDigit singleDigit = SingleDigit.builder().digit("9875").concatenation(4).build();
        singleDigitValidation.validate(singleDigit);
    }

    @Test
    public void validateDigitTypeInvalidDigitExpectThrowInvalidArgumentException(SingleDigitValidation sdv) {
        SingleDigit singleDigit = SingleDigit.builder().digit("98d75").concatenation(4).build();

        Assertions.assertThrows(InvalidArgumentException.class, () -> sdv.validate(singleDigit));
    }

    @Test
    public void validateDigitSize(SingleDigitValidation singleDigitValidation) {
        SingleDigit singleDigit = SingleDigit.builder().digit("1").concatenation(4).build();
        singleDigitValidation.validate(singleDigit);
    }

    @Test
    public void validateDigitSizeInvalidDigitMinimumSizeExpectThrowInvalidArgumentException(SingleDigitValidation sdv) {
        SingleDigit singleDigit = SingleDigit.builder().digit("0").concatenation(4).build();

        Assertions.assertThrows(InvalidArgumentException.class, () -> sdv.validate(singleDigit));
    }

    @Test
    public void validateDigitSizeMaxSizePermited(SingleDigitValidation sdv) {
        String s = new BigInteger("10").pow(1000000).add(BigInteger.valueOf(1)).toString();
        SingleDigit singleDigit = SingleDigit
                .builder()
                .digit(s)
                .concatenation(4)
                .build();

        Assertions.assertThrows(InvalidArgumentException.class, () -> sdv.validate(singleDigit));
    }

    @Test
    public void validateDigitSizeInvalidMaxSizeExpectThrowInvalidArgumentException(SingleDigitValidation sdv) {
        SingleDigit singleDigit = SingleDigit
                .builder()
                .digit(new BigInteger("10").pow(1000000).toString())
                .concatenation(4)
                .build();

        sdv.validate(singleDigit);
    }

    static class FooParameterResolver implements ParameterResolver {
        @Override
        public boolean supportsParameter(ParameterContext parameterContext,
                                         ExtensionContext extensionContext) throws ParameterResolutionException {
            return parameterContext.getParameter().getType() == SingleDigitValidation.class;
        }

        @Override
        public Object resolveParameter(ParameterContext parameterContext,
                                       ExtensionContext extensionContext) throws ParameterResolutionException {
            return new SingleDigitValidation();
        }
    }
}
