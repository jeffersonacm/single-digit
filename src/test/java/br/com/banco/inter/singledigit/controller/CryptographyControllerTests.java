package br.com.banco.inter.singledigit.controller;

import br.com.banco.inter.singledigit.domain.model.User;
import br.com.banco.inter.singledigit.domain.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CryptographyControllerTests {

    private final UserRepository userRepository;
    @LocalServerPort
    private int port;
    private final MockMvc mockMvc;
    private final String url = "/v1/cryptography";

    @Test
    @Order(1)
    public void findPublicKeyWhenReturn200Ok() throws Exception {
        User user = User.builder().email("person12331@hotmail.com").name("person").build();
        user = userRepository.save(user);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(url + "/{id}", user.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    @Order(2)
    public void encryptUserDataWhenReturn204NoContent() throws Exception {
        User user = User.builder().email("person123312@hotmail.com").name("person").build();
        user = userRepository.save(user);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(url + "/{id}", user.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();

        MockHttpServletRequestBuilder requestBuilder2 = MockMvcRequestBuilders
                .put(url + "/encrypt/{id}", user.getId())
                .content(response.getContentAsString())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult2 = mockMvc.perform(requestBuilder2).andReturn();
        MockHttpServletResponse response2 = mvcResult2.getResponse();

        Assertions.assertEquals(HttpStatus.NO_CONTENT.value(), response2.getStatus());
    }

    @Test
    @Order(3)
    public void decryptUserDataWhenReturn204NoContent() throws Exception {
        User user = User.builder().email("person1233123@hotmail.com").name("person").build();
        user = userRepository.save(user);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(url + "/{id}", user.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();

        MockHttpServletRequestBuilder requestBuilder2 = MockMvcRequestBuilders
                .put(url + "/encrypt/{id}", user.getId())
                .content(response.getContentAsString())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult2 = mockMvc.perform(requestBuilder2).andReturn();
        mvcResult2.getResponse();

        MockHttpServletRequestBuilder requestBuilder3 = MockMvcRequestBuilders
                .get(url + "/{id}", user.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult3 = mockMvc.perform(requestBuilder3).andReturn();
        mvcResult3.getResponse();

        MockHttpServletRequestBuilder requestBuilder4 = MockMvcRequestBuilders
                .put(url + "/decrypt/{id}", user.getId())
                .content(response.getContentAsString())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult4 = mockMvc.perform(requestBuilder4).andReturn();
        MockHttpServletResponse response4 = mvcResult4.getResponse();

        Assertions.assertEquals(HttpStatus.NO_CONTENT.value(), response4.getStatus());
    }

}
