package br.com.banco.inter.singledigit.controller;

import br.com.banco.inter.singledigit.domain.dto.UserDto;
import br.com.banco.inter.singledigit.domain.model.User;
import br.com.banco.inter.singledigit.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserControllerTests {

    @LocalServerPort
    private int port;
    private final MockMvc mockMvc;
    private final String url = "/v1/user";

    @Test
    @Order(1)
    public void saveWhenReturn201Created() throws Exception {
        UserDto userDto = UserDto.builder().email("user@user.com").name("person").build();

        UserService mock = org.mockito.Mockito.mock(UserService.class);
        Mockito.when(mock.save(Mockito.any(User.class))).thenReturn(userDto.toUser());

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(url)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDto))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        Assertions.assertNotNull(response.getHeaders(HttpHeaders.LOCATION).get(0));
    }

    @Test
    @Order(2)
    public void saveWhenReturn400BadRequestNoUniqueMail() throws Exception {
        UserDto userDto = UserDto.builder().email("person1@hotmail.com").name("person").build();

        UserService mock = org.mockito.Mockito.mock(UserService.class);
        Mockito.when(mock.save(Mockito.any(User.class))).thenReturn(userDto.toUser());

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(url)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDto))
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andReturn();
        MvcResult mvcResult2 = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response2 = mvcResult2.getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response2.getStatus());
    }

    @Test
    @Order(3)
    public void saveWhenReturn400BadRequestInvalidMail() throws Exception {
        UserDto userDto = UserDto.builder().email("personhotmail.com").name("person").build();

        UserService mock = org.mockito.Mockito.mock(UserService.class);
        Mockito.when(mock.save(Mockito.any(User.class))).thenReturn(userDto.toUser());

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(url)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDto))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }

    @Test
    @Order(4)
    public void updateWhenReturn204NoContent() throws Exception {
        UserDto userDto = UserDto.builder().email("user233@user.com").name("person").build();
        UserService mock = org.mockito.Mockito.mock(UserService.class);
        Mockito.when(mock.save(Mockito.any(User.class))).thenReturn(userDto.toUser());

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(url)
                .content(asJsonString(userDto))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();
        String header = response.getHeaders(HttpHeaders.LOCATION).get(0);
        String[] split = header.split("/");

        MockHttpServletRequestBuilder requestBuilder2 = MockMvcRequestBuilders
                .put(url + "/{id}", split[split.length-1])
                .content(asJsonString(userDto))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult2 = mockMvc.perform(requestBuilder2).andReturn();
        MockHttpServletResponse response2 = mvcResult2.getResponse();

        Assertions.assertEquals(HttpStatus.NO_CONTENT.value(), response2.getStatus());
    }

    @Test
    @Order(5)
    public void updateWhenReturn404NotFound() throws Exception {
        UserDto userDto = UserDto.builder().email("person@person1.com").name("person").build();

        UserService mock = org.mockito.Mockito.mock(UserService.class);
        Mockito.when(mock.save(Mockito.any(User.class))).thenReturn(userDto.toUser());

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put(url + "/{id}", 1000)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDto))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
    }

    @Test
    @Order(6)
    public void findUserByIdWhenReturnOk() throws Exception {
        UserDto userDto = UserDto.builder().email("person@hotmail.com").name("person").build();

        UserService mock = org.mockito.Mockito.mock(UserService.class);
        Mockito.when(mock.findById(1L)).thenReturn(java.util.Optional.ofNullable(userDto.toUser()));

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(url + "/{id}", 1);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    @Order(7)
    public void findUserByIdWhenReturnNotFound() throws Exception {
        UserDto userDto = UserDto.builder().email("person@hotmail.com").name("person").build();

        UserService mock = org.mockito.Mockito.mock(UserService.class);
        Mockito.when(mock.findById(1000L)).thenReturn(java.util.Optional.ofNullable(userDto.toUser()));

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(url + "/{id}", 1000);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
    }

    @Test
    @Order(8)
    public void findAllUserWhenReturnOk() throws Exception {
        List<User> list = new ArrayList<>();

        UserService mock = org.mockito.Mockito.mock(UserService.class);
        Mockito.when(mock.findAll(Pageable.unpaged())).thenReturn(list);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(url);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    @Order(9)
    public void deleteUserByIdWhenReturnNoContent() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete(url + "/{id}", 1);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatus());
    }

    @Test
    @Order(10)
    public void deleteUserByIdWhenReturnNotFound() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete(url + "/{id}", 1000);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
    }

    public static String asJsonString(final Object object) {
        String stringObj = null;

        try {
            stringObj = new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return stringObj;
    }

}
