package br.com.banco.inter.singledigit.domain.repository;

import br.com.banco.inter.singledigit.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;

import javax.validation.ConstraintViolationException;
import java.util.Collection;
import java.util.Optional;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserRepositoryTests {

    private final UserRepository userRepository;

    @Test
    @Order(1)
    public void findAllUsersShouldReturnTwoObjects() {
        User user = User.builder().name("1").email("1@hotmail.com").build();
        userRepository.save(user);

        User user2 = User.builder().name("2").email("2@hotmail.com").build();
        userRepository.save(user2);

        Iterable<User> all = userRepository.findAll();
        Assertions.assertEquals(2, ((Collection<?>) all).size());
    }

    @Test
    @Order(2)
    public void findUserByIdShouldReturnUser() {
        User user = User.builder().name("user").email("user@hotmail.com").build();
        userRepository.save(user);
        Optional<User> optionalUser = userRepository.findById(user.getId());

        Assertions.assertTrue(optionalUser.isPresent());
        Assertions.assertEquals("user", optionalUser.get().getName());
        Assertions.assertEquals("user@hotmail.com", optionalUser.get().getEmail());
    }

    @Test
    @Order(3)
    public void saveUserShouldPersist() {
        User user = User.builder().name("person").email("person@hotmail.com").build();
        userRepository.save(user);

        Assertions.assertNotNull(user.getId());
        Assertions.assertEquals("person", user.getName());
        Assertions.assertEquals("person@hotmail.com", user.getEmail());
    }

    @Test
    @Order(4)
    public void saveUserExpectErrorNotUniqueEmailThrowDataIntegrityViolationException() {
        User user = User.builder().name("person").email("person@hotmail.com").build();
        User user2 = User.builder().name("person").email("person@hotmail.com").build();
        userRepository.save(user);

        Assertions.assertThrows(DataIntegrityViolationException.class, () -> userRepository.save(user2));
    }

    @Test
    @Order(5)
    public void saveUserExpectErrorNullNameThrowConstraintViolationException() {
        User user = User.builder().name(null).email("person@hotmail.com").build();

        Assertions.assertThrows(ConstraintViolationException.class, () -> userRepository.save(user));
    }

    @Test
    @Order(6)
    public void saveUserExpectErrorNullEmailThrowConstraintViolationException() {
        User user = User.builder().name("person").email(null).build();

        Assertions.assertThrows(ConstraintViolationException.class, () -> userRepository.save(user));
    }

    @Test
    @Order(7)
    public void updateUserShouldPersist() {
        User user = User.builder().name("person").email("person@hotmail.com").build();
        userRepository.save(user);

        user.setEmail("newperson@hotmail.com");
        user.setName("newperson");
        userRepository.save(user);

        Assertions.assertNotNull(user.getId());
        Assertions.assertEquals("newperson", user.getName());
        Assertions.assertEquals("newperson@hotmail.com", user.getEmail());
    }

    @Test
    @Order(8)
    public void deleteUserShouldDelete() {
        User user = User.builder().name("person").email("person2@hotmail.com").build();
        userRepository.save(user);
        userRepository.delete(user);

        Optional<User> userDeleted = userRepository.findById(user.getId());

        Assertions.assertFalse(userDeleted.isPresent());
    }

}
