package br.com.banco.inter.singledigit.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.*;

@ExtendWith(CalculateDigitUtilTests.FooParameterResolver.class)
public class CalculateDigitUtilTests {


    @Test
    public void calculateDigit(CalculateDigitUtil calculateDigitUtil) {
        Integer digitCalculated = calculateDigitUtil.calculate("9875", 4);
        Assertions.assertEquals(8, digitCalculated);
    }

    @Test
    public void calculateDigit2(CalculateDigitUtil calculateDigitUtil) {
        Integer digitCalculated = calculateDigitUtil.calculate("9875", 1);
        Assertions.assertEquals(2, digitCalculated);
    }

    static class FooParameterResolver implements ParameterResolver {
        @Override
        public boolean supportsParameter(ParameterContext parameterContext,
                                         ExtensionContext extensionContext) throws ParameterResolutionException {
            return parameterContext.getParameter().getType() == CalculateDigitUtil.class;
        }

        @Override
        public Object resolveParameter(ParameterContext parameterContext,
                                       ExtensionContext extensionContext) throws ParameterResolutionException {
            return new CalculateDigitUtil();
        }
    }
}
