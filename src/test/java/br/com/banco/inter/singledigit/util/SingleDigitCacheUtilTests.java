package br.com.banco.inter.singledigit.util;

import br.com.banco.inter.singledigit.domain.model.SingleDigit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.*;

@ExtendWith(SingleDigitCacheUtilTests.FooParameterResolver.class)
public class SingleDigitCacheUtilTests {

    @Test
    public void addItemsExpectSearch(SingleDigitCacheUtil singleDigitCacheUtil) {
        SingleDigit singleDigit = SingleDigit.builder().digit("1").concatenation(1).digitCalculated(1).build();
        singleDigitCacheUtil.addItem(singleDigit);

        SingleDigit singleDigit2 = SingleDigit.builder().digit("2").concatenation(2).digitCalculated(2).build();
        singleDigitCacheUtil.addItem(singleDigit2);

        Assertions.assertEquals(singleDigit, singleDigitCacheUtil.searchItem(singleDigit));
    }

    @Test
    public void add10ItemsExpectSearch(SingleDigitCacheUtil singleDigitCacheUtil) {
        SingleDigit singleDigit = null;
        for (int i = 1; i <= 10; i ++) {
            singleDigit = SingleDigit.builder().digit(String.valueOf(i)).concatenation(i).digitCalculated(i).build();
            singleDigitCacheUtil.addItem(singleDigit);

            Assertions.assertEquals(singleDigit, singleDigitCacheUtil.searchItem(singleDigit));
        }
    }

    @Test
    public void add11ItemsExpectNotFindFirstInserted(SingleDigitCacheUtil singleDigitCacheUtil) {
        SingleDigit singleDigit = null;
        for (int i = 1; i <= 11; i ++) {
            singleDigit = SingleDigit.builder().digit(String.valueOf(i)).concatenation(i).digitCalculated(i).build();
            singleDigitCacheUtil.addItem(singleDigit);

        }

        singleDigit = SingleDigit.builder().digit("1").concatenation(1).digitCalculated(1).build();
        Assertions.assertEquals(null, singleDigitCacheUtil.searchItem(singleDigit));
    }

    static class FooParameterResolver implements ParameterResolver {
        @Override
        public boolean supportsParameter(ParameterContext parameterContext,
                                         ExtensionContext extensionContext) throws ParameterResolutionException {
            return parameterContext.getParameter().getType() == SingleDigitCacheUtil.class;
        }

        @Override
        public Object resolveParameter(ParameterContext parameterContext,
                                       ExtensionContext extensionContext) throws ParameterResolutionException {
            return new SingleDigitCacheUtil();
        }
    }
}
