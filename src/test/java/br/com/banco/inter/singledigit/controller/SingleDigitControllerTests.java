package br.com.banco.inter.singledigit.controller;

import br.com.banco.inter.singledigit.domain.dto.SingleDigitDto;
import br.com.banco.inter.singledigit.domain.dto.UserDto;
import br.com.banco.inter.singledigit.domain.model.SingleDigit;
import br.com.banco.inter.singledigit.domain.model.User;
import br.com.banco.inter.singledigit.domain.repository.SingleDigitRepository;
import br.com.banco.inter.singledigit.domain.repository.UserRepository;
import br.com.banco.inter.singledigit.service.SingleDigitService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Transactional
public class SingleDigitControllerTests {

    @LocalServerPort
    private int port;
    private final MockMvc mockMvc;
    private final String url = "/v1/singledigit";
    private final UserRepository userRepository;
    private final SingleDigitRepository singleDigitRepository;


    @Test
    @Order(1)
    public void saveWhenReturn201Created() throws Exception {
        SingleDigitDto singleDigitDto = SingleDigitDto.builder().digit("9875").concatenation(4).build();
        SingleDigit singleDigit = SingleDigit.builder().digit("9875").concatenation(4).digitCalculated(8).build();

        SingleDigitService mock = Mockito.mock(SingleDigitService.class);
        Mockito.when(mock.save(Mockito.any(SingleDigit.class))).thenReturn(singleDigit);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(url)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(singleDigitDto))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.CREATED.value(), response.getStatus());
    }

    @Test
    @Order(2)
    public void findDigitByUserWhenReturn200Ok() throws Exception {

        UserDto userDto = UserDto.builder().email("person2@hotmail.com").name("person").build();
        User user = userRepository.save(userDto.toUser());

        SingleDigit singleDigit = SingleDigit.builder().digit("9875").concatenation(4).digitCalculated(8).user(user).build();
        singleDigitRepository.save(singleDigit);

        SingleDigitService mock = Mockito.mock(SingleDigitService.class);
        Mockito.when(mock.save(Mockito.any(SingleDigit.class))).thenReturn(singleDigit);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(url+ "/user/{id}", user.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assertions.assertEquals("[" +asJsonString(singleDigit) + "]", response.getContentAsString());
    }

    @Test
    @Order(3)
    public void saveWhenReturn400BadRequestNoDigitParam() throws Exception {
        SingleDigitDto singleDigitDto = SingleDigitDto.builder().concatenation(4).build();
        SingleDigit singleDigit = SingleDigit.builder().digit("9875").concatenation(4).digitCalculated(8).build();

        SingleDigitService mock = Mockito.mock(SingleDigitService.class);
        Mockito.when(mock.save(Mockito.any(SingleDigit.class))).thenReturn(singleDigit);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(url)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(singleDigitDto))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }

    @Test
    @Order(4)
    public void saveByUserWhenReturn201Created() throws Exception {
        SingleDigitDto singleDigitDto = SingleDigitDto.builder().digit("9875").concatenation(4).build();
        SingleDigit singleDigit = SingleDigit.builder().digit("9875").concatenation(4).digitCalculated(8).build();

        SingleDigitService mock = Mockito.mock(SingleDigitService.class);
        Mockito.when(mock.save(Mockito.any(SingleDigit.class))).thenReturn(singleDigit);

        UserDto userDto = UserDto.builder().email("person43@hotmail.com").name("person").build();
        User user = userRepository.save(userDto.toUser());

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(url+ "/user/{id}", user.getId())
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(singleDigitDto))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.CREATED.value(), response.getStatus());
    }

    @Test
    @Order(5)
    public void findDigitCreatedByUserWhenReturn200OK() throws Exception {
        UserDto userDto = UserDto.builder().email("person3@hotmail.com").name("person").build();
        User user = userRepository.save(userDto.toUser());

        SingleDigit singleDigit = SingleDigit.builder().digit("9875").concatenation(4).digitCalculated(8).user(user).build();
        singleDigitRepository.save(singleDigit);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(url+ "/user/{id}", user.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assertions.assertEquals("[" + asJsonString(singleDigit) + "]", response.getContentAsString());
    }

    @Test
    @Order(6)
    public void findDigitCreatedByUserWhenReturn200OKEmpty() throws Exception {
        UserDto userDto = UserDto.builder().email("person4@hotmail.com").name("person").build();
        User user = userRepository.save(userDto.toUser());

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(url+ "/user/{id}", user.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assertions.assertEquals("[]", response.getContentAsString());
    }

    @Test
    @Order(7)
    public void saveByUserWhenReturn404NotFound() throws Exception {
        SingleDigitDto singleDigitDto = SingleDigitDto.builder().digit("9875").concatenation(4).build();
        SingleDigit singleDigit = SingleDigit.builder().digit("9875").concatenation(4).digitCalculated(8).build();

        SingleDigitService mock = Mockito.mock(SingleDigitService.class);
        Mockito.when(mock.save(Mockito.any(SingleDigit.class))).thenReturn(singleDigit);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(url+ "/user/{id}", 333)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(singleDigitDto))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();

        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
    }

    public static String asJsonString(final Object object) {
        String stringObj = null;

        try {
            stringObj = new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return stringObj;
    }

}
