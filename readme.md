# Desafio dígito único banco inter

## Pré-requisitos

- Java 1.8
- Maven 3.6 (ou posterior) 

## Instruções para compilação e execução

*Executar comandos na raiz do projeto*

Testes: `mvn test`

Executar aplicação via maven: `mvn spring-boot:run`

Executar aplicação pelo jar: `java -jar target/single-digit-1.0.jar` 


## Documentação

Swagger: `http://localhost:8080/api/swagger-ui.html`

Json collection postman: `Disponível na raiz do projeto`

## Desenvolvimento auxiliado pelas ferramentas

- Sonar lint
- IDEA intelij
- Lombok

###### Dicas e sujestões são bem vindas!

**Contato**

- Email `jeffersonacm@hotmail.com`